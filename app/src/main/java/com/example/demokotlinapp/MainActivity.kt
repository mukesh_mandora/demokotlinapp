package com.example.demokotlinapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var butTapMe: Button
    private lateinit var txtGameScore: TextView
    private lateinit var txtGameTimer: TextView
    private var score = 0
    private var isGameStarted = false
    private lateinit var countDownTimer: CountDownTimer
    private val initialCountDown: Long = 60000
    private val countDownInterval: Long = 1000
    private val TAG = MainActivity::class.java.simpleName
    private var timeLeftCountdown: Long = 60000

    companion object {

        private var SCORE_KEY = "SCORE_KEY"
        private var TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        butTapMe = findViewById(R.id.tap_button)
        txtGameScore = findViewById(R.id.score_textview)
        txtGameTimer = findViewById(R.id.timer_textview)

        Log.d(TAG, "on Create is called: $score")

        if (savedInstanceState != null) {

            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeftCountdown = savedInstanceState.getLong(TIME_LEFT_KEY)
            //Restore Game
            restoreGame()
        } else {
            //Reset Game
            resetGame()
        }


        butTapMe.setOnClickListener { view ->

            val bounceAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce)
            view.startAnimation(bounceAnimation)
            incrementScore()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.about) {
            showInfo()
        }
        return true
    }

    private fun showInfo() {

        val dialogTitle = getString(R.string.about_title)
        val dialogMessage = getString(R.string.about_message)

        var builder = AlertDialog.Builder(this)
        builder.setTitle(dialogTitle)
        builder.setMessage(dialogMessage)
        builder.show()

    }

    private fun restoreGame() {
        txtGameScore.text = getString(R.string.your_score, score.toString())
        var restoreTime = timeLeftCountdown / 1000
        txtGameTimer.text = getString(R.string.time_left, restoreTime.toString())

        countDownTimer = object : CountDownTimer(timeLeftCountdown, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftCountdown = millisUntilFinished
                var timeLeft = millisUntilFinished / 1000
                txtGameTimer.text = getString(R.string.time_left, timeLeft.toString())
            }

            override fun onFinish() {
                endGame()
            }

        }
        countDownTimer.start()
        isGameStarted = true
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putInt(SCORE_KEY, score)
        outState?.putLong(TIME_LEFT_KEY, timeLeftCountdown)

        countDownTimer.cancel()
    }


    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG, "on Destroy is called")

    }

    private fun resetGame() {

        //Initial to zero
        score = 0
        txtGameScore.text = getString(R.string.your_score, score.toString())
        txtGameTimer.text = getString(R.string.time_left, (initialCountDown / 1000).toString())

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftCountdown = millisUntilFinished
                var timeLeft = millisUntilFinished / 1000
                txtGameTimer.text = getString(R.string.time_left, timeLeft.toString())
            }

            override fun onFinish() {

                endGame()
            }

        }

        isGameStarted = false
    }


    private fun startGame() {
        countDownTimer.start()
        isGameStarted = true
    }

    private fun endGame() {
        Toast.makeText(this, getString(R.string.game_score, score.toString()), Toast.LENGTH_LONG).show()
        resetGame()
    }

    private fun incrementScore() {

        if (!isGameStarted) {
            startGame()
        }

        score += 1
        val newScore = getString(R.string.your_score, score.toString())
        txtGameScore.text = newScore

        val blinkAnimation = AnimationUtils.loadAnimation(this, R.anim.blink)
        txtGameScore.startAnimation(blinkAnimation)
    }

    private fun additionOfTwoNumbers(a: Int, b: Int): Int {
        return a + b
    }

    private fun subtraction(num1: Int, num2: Int): Int {
        return num1 - num2
    }

}
